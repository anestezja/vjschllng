module.exports = function(grunt) {

  grunt.initConfig({
    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [
          {
            cwd: "./templates",
            src: ["*.pug"],
            dest: "public/",
            expand: true,
            ext: ".html"
          }
        ],
      }
    },
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'public/css/style.css' : 'sass/style.sass'
        }
      }
    },
    // copy: {
    //   main: {
    //     expand: true,
    //     cwd: './public/templates/',
    //     src: ['index.html'],
    //     dest: 'public/',
    //   }
    // },
    // uglify: {
    //   my_target: {
    //     files: {
    //       'public/js/app.min.js': [ 'public/js/ng.js', 'public/js/controllers/controllers-module.js', 'public/js/controllers/**.js', '!public/js/controllers/controllers-module.js']
    //     }
    //   }
    // },
    watch: {
      js: {
        files: ['public/js/ng.js', 'public/js/controllers/**.js'],
        //tasks: ['uglify']
      },
      css: {
        files: 'sass/*.sass',
        tasks: ['sass'],
        options: {
          livereload: true,
        },
      },
      pug: {
        files: 'templates/*.pug',
        tasks: ['pug'],
        options: {
          livereload: true,
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-copy');
  //grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['pug', 'sass', 'watch']);
  //copy i uglify wyrzucilam
};
