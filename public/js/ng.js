const inventors = [
  { first: 'Albert', last: 'Einstein', year: 1879, passed: 1955 },
  { first: 'Isaac', last: 'Newton', year: 1643, passed: 1727 },
  { first: 'Galileo', last: 'Galilei', year: 1564, passed: 1642 },
  { first: 'Marie', last: 'Curie', year: 1867, passed: 1934 },
  { first: 'Johannes', last: 'Kepler', year: 1571, passed: 1630 },
  { first: 'Nicolaus', last: 'Copernicus', year: 1473, passed: 1543 },
  { first: 'Max', last: 'Planck', year: 1858, passed: 1947 },
  { first: 'Katherine', last: 'Blodgett', year: 1898, passed: 1979 },
  { first: 'Ada', last: 'Lovelace', year: 1815, passed: 1852 },
  { first: 'Sarah E.', last: 'Goode', year: 1855, passed: 1905 },
  { first: 'Lise', last: 'Meitner', year: 1878, passed: 1968 },
  { first: 'Hanna', last: 'Hammarström', year: 1829, passed: 1909 }
];

const people = ['Beck, Glenn', 'Becker, Carl', 'Beckett, Samuel', 'Beddoes, Mick',
 'Beecher, Henry', 'Beethoven, Ludwig', 'Begin, Menachem', 'Belloc, Hilaire', 'Bellow, Saul',
  'Benchley, Robert', 'Benenson, Peter', 'Ben-Gurion, David', 'Benjamin, Walter', 'Benn, Tony',
   'Bennington, Chester', 'Benson, Leana', 'Bent, Silas', 'Bentsen, Lloyd', 'Berger, Ric',
    'Bergman, Ingmar', 'Berio, Luciano', 'Berle, Milton', 'Berlin, Irving', 'Berne, Eric',
     'Bernhard, Sandra', 'Berra, Yogi', 'Berry, Halle', 'Berry, Wendell', 'Bethea, Erin',
      'Bevan, Aneurin', 'Bevel, Ken',
        'Biden, Joseph', 'Bierce, Ambrose', 'Biko, Steve', 'Billings, Josh', 'Biondo, Frank',
          'Birrell, Augustine', 'Black, Elk', 'Blair, Robert', 'Blair, Tony', 'Blake, William'];

// Array.prototype.filter()
// 1. Filter the list of inventors for those who were born in the 1500's
function isfrom(inventor) {
  return inventor.year >= 1500 && inventor.year < 1600;
};
const inventorsFrom = inventors.filter(isfrom);
console.log(inventorsFrom);

// Array.prototype.map()
// 2. Give us an array of the inventors' first and last names
const firstAndLast = function(inventor) {
  return {
    firstName: inventor.first,
    lastName: inventor.last
  }
}
const inventorsShort = inventors.map(firstAndLast);
console.log(inventorsShort);

// Array.prototype.sort()
// 3. Sort the inventors by birthdate, oldest to youngest
const birthdateSort = function(a, b) {
  return -(a.year - b.year);
}
const inventorsByBirthdate = inventors.sort(birthdateSort);
console.log(inventorsByBirthdate);

// Array.prototype.reduce()
// 4. How many years did all the inventors live?
const lifeLength = function(accumulator, currentItem) {
  return accumulator + currentItem.passed - currentItem.year;
}
const reducedInventors = inventors.reduce(lifeLength, 0)
console.log(reducedInventors);

// 5. Sort the inventors by years lived
const yearsLivedSort = function(a, b) {
  const aLived = a.passed - a.year;
  const bLived = b.passed - b.year;
  return aLived - bLived;
}
const inventorsByLifeLength = inventors.sort(yearsLivedSort);
console.log(inventorsByLifeLength);

// 6. create a list of Boulevards in Paris that contain 'de' anywhere in the name
// https://en.wikipedia.org/wiki/Category:Boulevards_in_Paris
// const container = document.querySelector('.mw-category');
// let links = container.querySelectorAll('a');
// let names = [];
//
// links.forEach(function(a) {
//   if(a.title.indexOf('de ') != -1) {
//     names.push(a.title);
//   }
// });
// console.log(names.length);

// 7. sort Exercise
// Sort the people alphabetically by last name
const sortByLastName = function(a, b) {
  const aLastIndex = a.indexOf(' ') + 1;
  const bLastIndex = b.indexOf(' ') + 1;

  const aLast = a.substring(aLastIndex, a.length - 1);
  const bLast = b.substring(bLastIndex, b.length - 1);

  if(aLast < bLast) {
    return 1;
  } else if (aLast > bLast) {
    return -1;
  } else {
    return 0;
  }
}
const peopleAlphabetically = people.sort(sortByLastName);
console.log(peopleAlphabetically);

// 8. Reduce Exercise
// Sum up the instances of each of these
const data = ['car', 'car', 'truck', 'truck', 'bike', 'walk',
 'car', 'van', 'bike', 'walk', 'car', 'van', 'car', 'truck' ];
