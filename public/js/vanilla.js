// function ask(question, yes, no) {
//   if (confirm(question)) yes()
//   else no();
// }
//
// ask(
//   "Do you agree?",
//   function() { alert("You agreed."); },
//   function() { alert("You canceled the execution."); }
// );

// console.log('To arrow:');
//
// let ask = (question, yes, no) => {
//   if (confirm(question)) {
//     return yes();
//   }
//   return no();
// }
//
// ask(
//   "Do you agree?",
//   () => alert("You agreed."),
//   () => alert("You canceled the execution.")
// );

let list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null
      }
    }
  }
};
console.log('Lexical environment check: ');
printList(list);

//
let check = function() {
  return 2;
}

let pow = function(x, n) {
  if(n < 0) return NaN;
  else if(Math.round(n) != n) return NaN;

  var y = x;
  for(var i = 0; i < (n - 1); i++) {
    x = x * y;
  }
  return x;
}



// square bracket notation
var user = {};
let key = 'kupa';
user[key] = true;
console.log(user);

// ^
let article = {
  title: 'Lorem ipsum',
  body: 'Dolor sit amet some body text'
};

//let a = prompt('Displaynąć title czy body?', 'title');

//alert(article[a]);

// dziala tez na klucze ze spacja i dziwnymi znakami, np.
let danuta = {
  'czy mam dinozaura na parapecie?': true
}
console.log(danuta['czy mam dinozaura na parapecie?']);
console.log('NO MAM');

// "property value shorthand"
function coZarazZrobie(film, platkiZMlekiem, papieros, spanie) {
  return {
    film,
    platkiZMlekiem,
    papieros,
    spanie
  }
};

let takBedzie = coZarazZrobie('Hobbit', true, 1, undefined);
console.log(takBedzie);

// existence check - ('key' in object)
if("film" in takBedzie) console.log("bedzie film");

// for ... in ... loop
for(key in takBedzie) {
  console.log(`Key: ${key}, value: ${takBedzie[key]}`);
}

// copying an object by object.assign
src1 = {
  key1: 1
}
src2 = {
  key2: 2
}
src3 = {
  key3: 3
}
destination = {}
Object.assign(destination, src1, src2, src3);
console.log(destination)
delete destination.key2;
console.log(destination);

// check for emptiness
function isEmpty(obj) {
  for(key in obj) {
    return false;
  }
  return true;
}

let schedule = {};

console.log(isEmpty(schedule));

schedule["8:30"] = "get up";

console.log(isEmpty(schedule));



// sum function
let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}

function salariesSum(obj) {
  let sum = 0;
  for(key in obj) {
    sum += obj[key];
  }
  return sum;
}

console.log(salariesSum(salaries));

// multiply numeric properties in an object
function multiplyNumeric(obj) {
  if(obj) {
    for(key in obj) {
      if(typeof(obj[key]) == "number") {
        obj[key] *= 2;
      }
    }
  }
}

// "this" calculator
let calculator = {
  read() {
    this.var1 = prompt("Pass the first variable");
    this.var2 = prompt("Pass the second variable");
  },
  sum() {
    return parseInt(this.var1) + parseInt(this.var2);
  },
  mul() {
    return this.var1 * this.var2;
  }
};

function log() {
  console.log(calculator);
}

    //calculator.read();
    //alert( calculator.sum() );
    //alert( calculator.mul() );

// "this" ladder
let ladder = {
  step: 0,
  up() {
    this.step++;
    return this;
  },
  down() {
    this.step--;
    return this;
  },
  showStep: function() { // shows the current step
    alert( this.step );
    return this;
  }
};

//ladder.up().up().down().showStep(); // 1

// new
function User(name, age) {
  this.name = name;
  this.age = age;
  this.isAdmin = false;
};

let michal = new User("Michal", 33);
console.log(michal);

// function User() {
//   alert(new.target);
// }

// without new:
//User(); // undefined

// with new:
//new User(); // function User { ... }

// objects equality
let obj = {};

function A() { return obj; };
function B() { return obj; };

let a = new A;
let b = new B;

console.log( a == b ); // true

// Calculators constructor

function Calculator() {
  this.read = function() {
    this.var1 = prompt("Pass the first variable");
    this.var2 = prompt("Pass the second variable");
  },
  this.sum = function() {
    return parseInt(this.var1) + parseInt(this.var2);
  },
  this.mul = function() {
    return this.var1 * this.var2;
  }
}

let calculator1 = new Calculator();

/* calculator1.read();
alert( "Sum=" + calculator1.sum() );
alert( "Mul=" + calculator1.mul() ); */

// Accumulator
function Accumulator(startingValue) {
  this.value = startingValue;
  this.read = function() {
    this.value += prompt("Read new value", 0);
  }
}

let accumulator = new Accumulator(1);

// Calculator with strings and new methods
// function Calculator1() {
//   this.calculate = function(str) {
//     let input = str.split(" ");
//     let var1 = parseInt(input[0]);
//     let var2 = parseInt(input[2]);
//     if(input[1] == "+") {
//       alert(var1 + var2);
//     } else if(input[1] == "-") {
//       alert(var1 - var2);
//     }
//   },
//   this.addMethod = function(operator, instruction) {
//
//   }
// }
//
//  powerCalc.addMethod("*", (a, b) => a * b);
// TO DO


// Math tasks
//prompt for two values and add them
function promptAndAdd() {
  let a = parseFloat(prompt("Pass the first value: "));
  let b = parseFloat(prompt("Pass the second value: "));
  alert(a + b);
}

//promptAndAdd();

// prompt until a number
function promptForNumber() {
  let a
  do {
    a = prompt("Enter a numeric value", 0);
    console.log(a);
  } while(!isFinite(a))
  if(a == null || a == "") return null;
  return +null;
}

// promptForNumber();

// rand min - max

function rand(min, max) {
  let a = Math.random() * min;
  let b = Math.random() * max;
  console.log(a);
  console.log(b);
  return b - a;
}
console.log(rand(2, 3));

//Arrays
let arr = [];
arr.age = 25;
console.log(arr);
// tak nie powinno byc
// push i pop dzialaja z koncem tablicy
// shift i unshift dzialaja z poczatkiem; sa wolniejsze, bo trzeba przenumerowac pozostale elementy tablicy
// FOR ... OF ... zamiast for(i; i<...; i++) dla tablicy
// FOR ... IN ... jest do obiektow, bo iteruje przez wszystkie wlasnosci
// tablic nie nalezy uzywac jak obiektow (arr.age = 25 jest zle)
// array.length to nie dlugosc tablicy, tylko index ostatniego elementu + 1
let fruits = ["Banana", "Orange", "Apple", "Pear", "Plum", "Lemon", "Watermelon", "Grape", "Strawberry"];
for(let fruit in fruits) {
  console.log(fruits[fruit]);
}
arr.push('down');
console.log(arr);

// array.length mozna nadac z palca
console.log(fruits.length);
fruits.length = 8;
console.log(fruits); // UCIELO OSTATNI ELEMENT
// array.length = 0 ladnie czysci tablice

// ZADANKA NA TABLICE i ICH METODY

let styles = ["Jazz", "Blues"];
console.log(styles);
styles.push("Rock-n-Roll");
console.log(styles);
let half = Math.floor(styles.length/2);
styles[half] = "Classics";
console.log(styles);
console.log(styles.shift());
console.log(styles);
styles.unshift("Rap", "Reggie");
console.log(styles);

// sum input numbers
function whatever() {
  let sumArr = [];
  function sumInput() {
    let a = prompt("Pass a number", "");
    if (isFinite(a) && a != "" && a != null) {
      sumArr.push(a);
      console.log(sumArr);
      sumInput();
    } else {
      let sum = 0;
      for (let number of sumArr) {
        sum += parseInt(number);
      }
      console.log(sum);
    }
  }

  sumInput();
}


// // max sum of subarray
// function sumSubArr(i, arr) {
//   sum = 0;
//   for (var j = arr.length; j > i; j--) {
//     sum += arr[arr.length - j]
//   }
//   return sum;
// }
//
// let sumArr = [];
// function getMaxSubSum(arr) {
//   for(var i = 0; i < arr.length; i++) {
//     sum = sumSubArr(i, arr);
//     sumArr.push(sum);
//   }
// }

// array find

let users = [
  {id: 1, name: "John"},
  {id: 2, name: "Pete"},
  {id: 3, name: "Mary"}
];

let result = users.find(function(element) {
  return element.id == 3;
});
console.log(result);

// array filter

let result1 = users.filter(function(element) {
  return element.id < 4;
});
console.log(result1);

// array map
let result2 = users.map(function(element) {
  return element.id = element.id + 1;
});
console.log(users);

// array.sort dziala na stringach, wiec 1, 15, 2 jest posortowane
// trzeba dac swoj callback, zeby sortowala na liczbahch

function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a == b) return 0;
  if (a < b) return -1;
}

let numbers = [1, 14, 89, 43, 8, 0, 2, 1, 43];

// numbers.sort(compareNumeric);
// console.log(numbers);

// turbokrotka wersja funkcji sortujacej, ktora potrzebuje tylko dodatniej lub ujemnej wartosci, nie konkretnie "-1" lub "1"

numbers.sort((a, b) => a - b);
console.log(numbers);

// array reverse
numbers.reverse();
console.log(numbers);

// string to array
// jak dam pusty separator do ta tablice pojedynczych znakow
let colors = "blue, yellow, red, orange, pink, green, purple";

let colorsArray = colors.split(",");
console.log(colorsArray);

// forEach, ale niech nie alertuje
//["Bilbo", "Gandalf", "Nazgul"].forEach(alert);

// isArray
console.log(Array.isArray(["Bilbo", "Gandalf", "Nazgul"]));

// thisArg for array methods
// becomes "this" for the callback functions

// border-left-width to bodredLeftWidth

// function camelize(str) {
//   if (str == "") return "";
//   let stringArray = str.split('-');
//   let japierdole = stringArray.map(function(item) {
//     let itemString = item.split('');
//     itemString[0] = itemString[0].toUpperCase();
//     let stringResult = itemString.join('');
//     return stringResult;
//   });
//   let result = japierdole.join('');
//   console.log(result);
// }

function camelize(str) {
  if (str == "") return "";

  function findIndexes(str) {
    let toUpperIndexes = [];
    let pos = 0;
    while(true) {
      let foundPos = str.indexOf('-', pos);
      if(foundPos == -1) {
        return toUpperIndexes;
      } else {
        toUpperIndexes.push(foundPos);
      }
      pos = foundPos + 1;
    }

  }
  let toUpperIndexes = findIndexes(str);
  let stringArray = str.split('');
  toUpperIndexes.forEach(function(item) {
    stringArray[item + 1] = stringArray[item + 1].toUpperCase();
  });

  console.log('for loop: ');
  for(var i = 0; i < toUpperIndexes.length; i++) {
    stringArray.splice(toUpperIndexes[i] - i, 1);
  }

  let result = stringArray.join('');
  console.log(result);
  return result;
}


// function camelize(str) {
//   let chuj = str.split('-');
//     console.log(chuj);
//   let dupa = chuj.map(
//       (word, index) => index == 0 ? word : word[0].toUpperCase() + word.slice(1)
//     );
//     console.log(dupa);
//   let japierdole =  dupa.join('');
//   console.log(japierdole);
//     return japierdole;
// }

camelize("background-color-chuj-dupa");

// filter in place
function filterRange(arr, a, b) {
  let result = [];
  for(let item of arr) {
    if (a <= item && item <= b)
    result.push(item);
  }
  console.log(result);
  return result;
};

let exampleArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

filterRange(exampleArray, 3, 7);

// filter range in place

function filterRangeInPlace(arr, a, b) {
  var i;
  for (i = 0; i < arr.length; i++) {
    if(a > arr[i]) {
      console.log(`${arr[i]} mniejsze od ${a}`);
      arr.splice(i, 1);
      i--;
    } else if (arr[i] > b) {
      console.log(`${arr[i]} wieksze od ${b}`);
      arr.splice(i, 1);
      i--;
    }
  }
};

console.log("filter range in place: ");
let exampleArray1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
filterRangeInPlace(exampleArray1, 3, 4);

// sort in reverse order

let reverseSortArray = [5, 2, 1, -10, 8];

function reverseSort(arr) {
  arr.sort((a, b) => a - b);
  arr.reverse();
}; // TU JEST ZLE, BO MIALO BYC ODWROCONE, OD NAJWIEKSZYCH DO NAJMNIEJSZYCH

// PROSCIEJ BY BYLO W JEDNEJ LINIJCE, CO, DANUTA
function prosciejReverse(arr) {
  arr.sort((a, b) => b - a);
}

reverseSort(reverseSortArray);
console.log(reverseSortArray);

// sorted copy of an array
let techArray = ["HTML", "JavaScript", "CSS"];
function copySorted(arr) {
  return arr.slice().sort();
}
let sorted = copySorted(techArray);
techArray.push("dupa");
console.log(sorted);
console.log(techArray);

// map to names
function mapToNames(arr) {
  let result = arr.map(function(item) {
    return item.name;
  });
  return result;
}
let john = { name: "John", age: 25 };
let pete = { name: "Pete", age: 30 };
let mary = { name: "Mary", age: 28 };

let users1 = [ john, pete, mary ];

let names1 = mapToNames(users1);
console.log(names1);

// lepsze rozwiazanie:
let names2 = users1.map(item => item.name);
console.log(names2);

// map to objects
let john1 = { name: "John", surname: "Smith", id: 1 };
let pete1 = { name: "Pete", surname: "Hunt", id: 2 };
let mary1 = { name: "Mary", surname: "Key", id: 3 };

let users2 = [ john1, pete1, mary1 ];

let usersMapped = users2.slice();
usersMapped = usersMapped.map(item => ({id: item.id, fullName: `${item.name} ${item.surname}`}));
console.log(usersMapped);

// sort objects
let john2 = { name: "John", age: 25 }
let pete2 = { name: "Pete", age: 30 }
let mary2 = { name: "Mary", age: 28 }
let mary12 = { name: "Dupa", age: 13 }
let mary22 = { name: "Lorem", age: 99 }

let nameSortArray = [ john2, pete2, mary2, mary12, mary22 ];

console.log("sort objects: ");
function sortByName(arr) {
  arr.sort((a, b) => a.name > b.name);
}
sortByName(nameSortArray);
console.log(nameSortArray);

// shuffle an array
let arrayToShuffle = [1, 2, 3, 4, 5, 6];
function shuffle(arr) {
  arr.sort(function(a, b) {
    let result =  Math.random();
    if(0 <= result < 0.33) return 0;
    if(0.33 <= result < 0.66) return 1;
    if(0.66 <= result <= 1) return -1;
  });
}
shuffle(arrayToShuffle);
console.log(arrayToShuffle);
shuffle(arrayToShuffle);
console.log(arrayToShuffle);
shuffle(arrayToShuffle);
console.log(arrayToShuffle);

// get average age, nameSortArray
// let value = arr.reduce(function(previousValue, item, index, arr) {
//   // ...
// }, initial);

let averageAge = (nameSortArray.reduce((sum, item) => sum + item.age, 0)) / (nameSortArray.length);
console.log(averageAge);

// sum the objects properties
let salaries1 = {
  "John": 100,
  "Pete": 300,
  "Mary": 250
};

function sumSalaries(obj) {
  let result = 0
  for(let salary of Object.values(obj)) {
    if(!isNaN(salary)) result += salary;
  }
  return result;
}

let totalSalaries = sumSalaries(salaries1);
console.log(totalSalaries);

// count objects properties
function count(obj) {
  return Object.values(obj).length;
}
console.log(count(salaries1));

// DATE Object
let now = new Date();
console.log(now);
let dateString = new Date('2053-11-09');
console.log(dateString);
let dateParams = new Date(1994, 1, 16, 21, 43);
console.log(dateParams);
console.log(now.getFullYear());
console.log(`dzis: ${now.getDay()}; 0 to niedziela, 6 to sobota`);

// Date.now() daje timestampo 01.01.1970 w milisekundach
// mozna sobie mierzyc czas wykonania funkcji, a substrakcja jest wykonywana na liczbach,
// a nie obiektach, wiec jest szybciej

function measureTime() {
  let start = Date.now();
  let count = 0;
  for(let i = 0; i < 10000; i++) {
    i = i * i;
    count++;
  }
  let end = Date.now();
  console.log(`it took ${end - start} ms and ${count} loops`);
};
measureTime();

// Create a date
let createDate = new Date(2012, 1, 20, 3, 12);
console.log(createDate);

// Show a weekday
function getWeekDay(date) {
  let day = date.getDay();
  switch(day) {
    case 00:
      return 'SU';
      break;
    case 01:
      return 'MO';
      break;
    case 02:
      return 'TU';
      break;
    case 03:
      return 'WE';
      break;
    case 04:
      return 'TH';
      break;
    case 05:
      return 'FR';
      break;
    case 06:
      return 'SA'
      break;
    default:
      return null;
  }
};

console.log(getWeekDay(new Date(2012, 0, 3)));

// European weekday
function getLocalDay(date) {
  let european = [7, 1, 2, 3, 4, 5, 6];
  return european[date.getDay()];
}

console.log(getLocalDay(new Date()));

// day of month many days ago
function getDateAgo(date, days) {
  let day = new Date(date.getFullYear(), date.getMonth(), date.getDate() - days);
  return day.getDate();
};
getDateAgo(new Date(2015, 0, 2), 2)

// get the last day of month
function getLastDayOfMonth(year, month) {
  let day = new Date(year, month + 1, 1);
  day.setDate(day.getDate() - 1);
  return day.getDate();
}

console.log(getLastDayOfMonth(2012, 1));

// seconds that passed today
function getSecondsToday() {
  let start = new Date();
  start.setHours(0, 0, 0, 0);
  let now = new Date();
  return now - start;
}

console.log(getSecondsToday());

// Format the relative date
function formatDate(date) {
  let diff = new Date() - date;
  if (diff < 1000) {
    return 'right now';
  } else if (1000 <= diff && diff < 1000 * 60) {
    return `${diff/1000} sec. ago`;
  } else if (1000* 60 <= diff && diff < 1000 * 60 * 60) {
    return `${diff/1000/60} min. ago`;
  } else {
    let dateArray = [
      '0' + date.getDate(),
      '0' + (date.getMonth() + 1),
      '' + date.getFullYear(),
      '0' + date.getHours(),
      '0' + date.getMinutes(),
    ].map(item => item.slice(-2));
    let first = dateArray.slice(0, 3).join('.');
    let second = dateArray.slice(3, 5).join(':');

    console.log(date.getMonth());
    return `${first} ${second}`;
  }
}
console.log(formatDate(new Date(2014, 2, 1, 11, 22, 33)));
//formatDate(new Date())

// turn an object into JSON and back
let user3 = {
  name: "John Smith",
  age: 35
};

let userJSON = JSON.stringify(user3);
console.log(userJSON);
let userObject = JSON.parse(userJSON);
console.log(userObject);

// stringify excluding circular references
let room = {
  number: 23
};

let meetup = {
  title: "Conference",
  occupiedBy: [{name: "John"}, {name: "Alice"}],
  place: room
};

// circular references
room.occupiedBy = meetup;
meetup.self = meetup;

// alert( JSON.stringify(meetup, function replacer(key, value) {
//   if(key != '' && value == meetup) return undefined;
//   return value;
// }));

// recursion power
function pow1(a, b) {
  if(b == 0) return 1;
  if(b == 1) return a;
  return a * pow(a, b - 1);
}
console.log(pow1(2, 3));
console.log(pow1(5, 7));

// summ all the numbers till the given one
function sumTo(a) {
  if (a == 1) return 1;
  return a + sumTo(a-1);
}
console.log(sumTo(100));

// calculate factorial
function factorial1(a) {
  if(a <= 1) return 1;
  return a * factorial1(a-1);
};
console.log(factorial1(8));

// fibonacci numbers
function fib(a) {
  if (a == 1 || a == 0) return 1;
  return fib(a - 1) + fib(a-2);
};
//console.log(fib(77));

// linked list output
// let list = {
//   value: 1,
//   next: {
//     value: 2,
//     next: {
//       value: 3,
//       next: {
//         value: 4,
//         next: null
//       }
//     }
//   }
// };

console.log('printList console.dir:');
console.dir(printList);
function printList(list) {

  console.log(list.value);
  if(list.next != null) return printList(list.next);
}
printList(list);


// closures
/*let name = "John";

function sayHi() {
  alert("Hi, " + name);
}

name = "Pete";

//sayHi(); */

/* function makeWorker() {
  let name = "Pete";

  return function() {
    alert(name);
  };
}

let name = "John";

// create a function
let work = makeWorker();

// call it
work(); */

// console.log('leetcode array task: ');
// // leetcode array task
// var twoSum = function(nums, target) {
//     let result = [];
//     for(var i = 0; i < nums.length; i++) {
//       juz mi sie nie chce tego pisac, ale nie zadzialalo z forEachem, bo
//       byl return z petli dla kazdego elementu
//     }
//
//
//     nums.forEach(function(item, index) {
//         for(var i = (index + 1); i < nums.length; i++) {
//             if(item + nums[i] == target) {
//               return result.push(index, i);
//             }
//         }
//     })
// };
//
// let testNums = [2, 7, 11, 15];
// let target = 9;
//
// console.log(`twoSum() call: ${twoSum(testNums, target)}`);

// yt closure test
// to tak działa, bo addTo zwraca funkcję, której podaję parametr w console.logu
function addTo(number1) {

  return function(number2) {
    return number1 + number2;
  }
}

let addToOne = addTo(1);
let addToTwo = addTo(2);

console.log(`addToOne(2): ${addToOne(2)}`);
console.log(`addToTwo(2): ${addToTwo(2)}`);

// Output every second with setTimeout

// function printNumbersTimeout(from, to) {
//   if(from <= to) {
//     console.log(from);
//     from++;
//     setTimeout(printNumbersInterval, 1000, from, to);
//   }
// }
//
// printNumbersTimeout(10, 40);

// Output every second with setInterval

function printNumbersInterval(from, to) {
  let i = from;
  function dupa() {
    if (i <= to) {
      console.log(i);
      i++;
    }
  }

  setInterval(dupa, 1000);
};

printNumbersInterval(50, 80);

// exports for tests
if(typeof exports !== 'undefined') {
    exports.testFunction = check;
    exports.powFunction = pow;
    exports.isEmpty = isEmpty;
    exports.multiplyNumeric = multiplyNumeric;
    exports.calculator = calculator;
    exports.ladder = ladder;
    exports.filterRangeInPlace = filterRangeInPlace;
}
