var chai = require('chai'),
    assert = require('assert'),
    vanillaJS = require('./public/js/vanilla');

describe('check()', function() {
  it('should return 2', function() {
    assert.equal(vanillaJS.testFunction(), 2);
  });
});

describe('pow(x, n)', function() {

  before(() => console.log('Tests start here!'));
  after(() => console.log('Tests end here :3'));

  beforeEach(() => console.log('Before each <3'));
  afterEach(() => console.log('After each'));

  it('test 1', () => console.log('test 1, halo?'));
  it('test 2', () => console.log('test 2, ZGLASZAM SIE!'));
  it('test 3', () => console.log('test 3, TO SA JUZ TESTY, a nie funkcje do nich! SZOK!'));

  function powTest(x) {
    let expected = x * x * x;
    it(`${x} to the power of 3 should return ${expected}`, function() {
      assert.equal(vanillaJS.powFunction(x, 3), expected);
    });
  }

  for(let i = 0; i < 5; i++) {
    powTest(i);
  }

  it('should return NaN if the power is negative', function() {
    chai.assert.isNaN(vanillaJS.powFunction(3, -2));
  })

  it('should return NaN if the power is not an integer', function() {
    chai.assert.isNaN(vanillaJS.powFunction(3, 1.2));
  })
});

describe("isEmpty", function() {
  it("returns true for an empty object", function() {
    chai.assert.isTrue(vanillaJS.isEmpty({}));
  });

  it("returns false if a property exists", function() {
    chai.assert.isFalse(vanillaJS.isEmpty({
      anything: false
    }));
  });
});

describe("multiplyNumeric", function() {
  it("multiplies all numeric properties by 2", function() {
    let menu = {
      width: 200,
      height: 300,
      title: "My menu"
    };
    let result = vanillaJS.multiplyNumeric(menu);
    chai.assert.equal(menu.width, 400);
    chai.assert.equal(menu.height, 600);
    chai.assert.equal(menu.title, "My menu");
  });

  it("returns nothing", function() {
    chai.assert.isUndefined( vanillaJS.multiplyNumeric({}) );
  });

});

// describe("calculator", function() {
//
//   context("when 2 and 3 entered", function() {
//     beforeEach(function() {
//       sinon.stub(window, "prompt");
//
//       prompt.onCall(0).returns("2");
//       prompt.onCall(1).returns("3");
//
//       vanillaJS.calculator.read();
//     });
//
//     afterEach(function() {
//       prompt.restore();
//     });
//
//     it("the sum is 5", function() {
//       chai.assert.equal(vanillaJS.calculator.sum(), 5);
//     });
//
//     it("the multiplication product is 6", function() {
//       chai.assert.equal(vanillaJS.calculator.mul(), 6);
//     });
//   });
//
// });

// describe('Ladder', function() {
//   let ladder = vanillaJS.ladder;
//
//   before(function() {
//     window.alert = sinon.stub(window, "alert");
//   });
//
//   beforeEach(function() {
//     ladder.step = 0;
//   });
//
//   it('up() should return this', function() {
//     assert.equal(ladder.up(), ladder);
//   });
//
//   it('down() should return this', function() {
//     assert.equal(ladder.down(), ladder);
//   });
//
//   it('showStep() should call alert', function() {
//     ladder.showStep();
//     assert(alert.called);
//   });
//
//   it('up() should increase step', function() {
//     assert.equal(ladder.up().up().step, 2);
//   });
//
//   it('down() should decrease step', function() {
//     assert.equal(ladder.down().step, -1);
//   });
//
//   it('down().up().up().up() ', function() {
//     assert.equal(ladder.down().up().up().up().step, 2);
//   });
//
//   after(function() {
//     ladder.step = 0;
//     alert.restore();
//   });
// });

describe("filterRangeInPlace", function() {

  it("returns the filtered values", function() {

    let arr = [5, 3, 8, 1];

    vanillaJS.filterRangeInPlace(arr, 1, 4);

    chai.assert.deepEqual(arr, [3, 1]);
  });

  it("doesn't return anything", function() {
    chai.assert.isUndefined(vanillaJS.filterRangeInPlace([1,2,3], 1, 4));
  });

});
